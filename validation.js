const Joi = require('@hapi/joi')
const registerValidation = (data) => {
  const schema = Joi.object({
    name: Joi.string()
      .min(6)
      .required(),
    email: Joi.string()
      .min(6)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .required()
  })
  return schema.validate(data)
  
  
}
const loginValidation = (data) => {
  const schema = Joi.object({
    email: Joi.string()
      .min(6)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .required()
  })
  return schema.validate(data)
}

const createSurveyValidation = (data) => {
    let questions=Joi.object().keys({
        question: Joi.string().min(10),
        a:Joi.string().min(3),
        b:Joi.string().min(3),
        c:Joi.string().min(3),
        d:Joi.string().min(3),
      })
    const schema = Joi.object({
        user_id:Joi.string()
        .min(3)
        .required(),
        surveyName: Joi.string()
        .min(3)
        .required(),
        isOpen:Joi.string()
        .min(3).required(),
      questions: Joi.array().items(questions)
    })
    return schema.validate(data)
  }
  const surveyResponseValidation = (data) => {
    let answers=Joi.object().keys({
        question: Joi.string().min(10),
        answer:Joi.string().min(1),
        })
    const schema = Joi.object({
        survey_id:Joi.string()
        .min(3)
        .required(),
        email: Joi.string()
        .min(6)
        .required()
        .email(),
        answers: Joi.array().items(answers)
    })
    return schema.validate(data)
  }

module.exports.registerValidation = registerValidation

module.exports.loginValidation = loginValidation
module.exports.createSurveyValidation = createSurveyValidation
module.exports.surveyResponseValidation = surveyResponseValidation

