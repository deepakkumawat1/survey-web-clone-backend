const express = require('express')
const app = express()
const authRoute = require('./routes/auth')
var bodyParser = require('body-parser')
const postRoute = require('./routes/posts') 
const createSurveysRoute = require('./routes/createSurveys')
const surveyResponseRoute = require('./routes/surveyResponse')


const mongoose = require('mongoose')
const dotenv = require('dotenv')
var cors = require('cors')
dotenv.config()     
mongoose.connect(
  process.env.DB_CONNECT,
  { useNewUrlParser: true ,useUnifiedTopology: true},
).catch(error => console.log(error));
mongoose.connection.on('connected', () => {
    console.log('mongoose is connect..');
  });
app.use(cors()) 
app.use(express.json({extended:false}))
app.use(bodyParser.urlencoded({
    extended: true
  }));
app.use('/api/user', authRoute)
app.use('/api/posts', postRoute)
app.use('/surveyResponse', surveyResponseRoute)

app.use('/createSurveys', createSurveysRoute)
// app.use('/surveyResponse', surveyResponseRoute)

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});


app.listen( process.env.PORT || 3010, () => console.log('server up and running'))
