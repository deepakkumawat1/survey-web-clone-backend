const router= require('express').Router();
const SurveyResponse= require("../model/Response")
const {surveyResponseValidation} = require('../validation')

router.post('/:surveyId',async(req,res) =>{
    const surveyResponseData={survey_id:req.params.surveyId,...req.body}
    console.log(surveyResponseData)
    const { error } = surveyResponseValidation(surveyResponseData)
    if(error) {
        return res.status(200).send({error:error.details[0].message})
      }
      const surveyResponse= new SurveyResponse(surveyResponseData)
      try{
        const savedResponse= await surveyResponse.save();
        res.status(200).send({ message : 'Successfully Saved Survey Response',response:savedResponse})
    }catch(err){
        res.status(400).send(err)
    }
})
module.exports= router;



