const router= require('express').Router();
const User= require("../model/User")
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');
const {loginValidation,registerValidation} = require('../validation')
// import { User } from "../model/User";





router.post('/register',async(req,res) =>{
    const { error } = registerValidation(req.body)
    if (error) {
        return res.status(200).send({error:error.details[0].message})
      }
    const emailExist= await User.findOne({email: req.body.email})
    if(emailExist) return res.status(200).send({ error : 'Email already exist'})

    const salt = await bcrypt.genSalt(10);
    const hashedPassword= await bcrypt.hash(req.body.password,salt)
    const user =new User({
        name:req.body.name,
        email:req.body.email,
        password:hashedPassword
    })
    try{
        const savedUser= await user.save();
        res.status(200).send({ message : 'Successfully signed up',user: user._id})
    }catch(err){
        res.status(400).send(err)
    }
    res.send('Register');
    console.log("successfull")
})
router.post('/login',async (req,res) => {
    const {error} = loginValidation(req.body)
    if(error) return res.status(200).send({error: error.details[0].message})
    const user= await User.findOne({email: req.body.email})
    if(!user) return res.status(200).send({error:'Email or password is wrong'})

    const validPass = await bcrypt.compare(req.body.password,user.password);
    if(!validPass) return res.status(200).send({error:'Invalid Password '});
    const token = jwt.sign({_id:user._id},process.env.TOKEN_SECRET);
    res.header('auth-token', token).status(200).send({token}) 
    // res.status(200).send('Successfully Logged In !')
})

module.exports= router;
