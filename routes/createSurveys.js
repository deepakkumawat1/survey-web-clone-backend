const router= require('express').Router();
const UserSurveys= require("../model/UserSurveys")
const {createSurveyValidation} = require('../validation')

router.post('/:userId',async(req,res) =>{
    const userSurveyData={user_id:req.params.userId,...req.body}
    console.log(userSurveyData)
    const { error } = createSurveyValidation(userSurveyData)
    if(error) {
        return res.status(200).send({error:error.details[0].message})
      }
      const userSurveys= new UserSurveys(userSurveyData)
      try{
        const savedUser= await userSurveys.save();
        res.status(200).send({ message : 'Successfully Saved Survey',response:savedUser})
    }catch(err){
        res.status(400).send(err)
    }
})
module.exports= router;
