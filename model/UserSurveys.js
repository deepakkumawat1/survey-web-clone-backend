const mongoose=require('mongoose')
const userSurveySchema = new mongoose.Schema({
    user_id:{
        type:String,
        required:true,
    },
    surveyName:{
        type:String,
        required:true,
    },
    isOpen:{
        type:Boolean,
        required:true
    },
    questions: [
        {
            question: String,
            a: String,
            b: String,
            c: String,
            d: String,
        },
        {
            question: String,
            a: String,
            b: String,
            c: String,
            d: String,
        },
        {
            question: String,
            a: String,
            b: String,
            c: String,
            d: String,
        },
        {
            question: String,
            a: String,
            b: String,
            c: String,
            d: String,
        },
        {
            question: String,
            a: String,
            b: String,
            c: String,
            d: String,
        },
        {
            question: String,
            a: String,
            b: String,
            c: String,
            d: String,
        },
        {
            question: String,
            a: String,
            b: String,
            c: String,
            d: String,
        },
        {
            question: String,
            a: String,
            b: String,
            c: String,
            d: String,
        },
        {
            question: String,
            a: String,
            b: String,
            c: String,
            d: String,
        },
        {
            question: String,
            a: String,
            b: String,
            c: String,
            d: String,
        },
    ],
    date:{
        type:Date,
        default:Date.now
    }
});
module.exports = mongoose.model('UserSurveys',userSurveySchema);