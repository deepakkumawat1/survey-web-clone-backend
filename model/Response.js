const mongoose=require('mongoose')
const surveyResponseSchema = new mongoose.Schema({
    survey_id:{
        type:String,
        required:true,
    },
    email:{
        type:String,
        required:true,
    },
    answers: [
        {
            question: String,
            answer: String,
        },
        {
            question: String,
            answer: String,
        },
        {
            question: String,
            answer: String,
        },
        {
            question: String,
            answer: String,
        },
        {
            question: String,
            answer: String,
        },
        {
            question: String,
            answer: String,
        },
        {
            question: String,
            answer: String,
        },
        {
            question: String,
            answer: String,
        },
        {
            question: String,
            answer: String,
        },
        {
            question: String,
            answer: String,
        },
    ],
    date:{
        type:Date,
        default:Date.now
    }
});
module.exports = mongoose.model('SurveyResponse',surveyResponseSchema);